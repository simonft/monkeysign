#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2012-2013 Antoine Beaupré <anarcat@orangeseeds.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from glob import glob
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import monkeysign
import monkeysign.documentation
import monkeysign.build_manpage
import monkeysign.translation

setup(name='monkeysign',
      description=monkeysign.__description__,
      long_description=monkeysign.__long_description__,
      use_scm_version={
          'write_to': 'monkeysign/_version.py',
      },
      setup_requires=['sphinx', 'setuptools_scm'],
      install_requieres=['pysocks'],
      author='Antoine Beaupré',
      author_email='anarcat@debian.org',
      url=monkeysign.__website__,
      packages=['monkeysign', 'monkeysign.tests'],
      package_data={
          'monkeysign.tests': ['files/*'],
      },
      scripts=['scripts/monkeysign', 'scripts/monkeyscan'],
      cmdclass={'build_manpage': monkeysign.build_manpage.BuildManPage,
                'build_trans': monkeysign.translation.build_trans,
                'build_slides': monkeysign.documentation.build_slides,
                },
      data_files=[('share/man/man1', glob('man/*.1')),
                  ('share/pixmaps', ['doc/ui-mockups/logo.simple.png']),
                  ('share/icons/hicolor/scalable',
                   ['doc/ui-mockups/logo.simple.svg']),
                  ],
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Information Technology',
          'Intended Audience :: Legal Industry',
          'Intended Audience :: Telecommunications Industry',
          'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.4',
          'Programming Language :: Python :: 2.5',
          'Programming Language :: Python :: 2.6',
          'Programming Language :: Python :: 2.7',
          'Operating System :: OS Independent',
          'Environment :: X11 Applications :: GTK',
          'Environment :: Console',
          'Natural Language :: English',
          'Natural Language :: French',
          'Natural Language :: Italian',
          'Natural Language :: Spanish',
          'Topic :: Communications :: Email',
          'Topic :: Multimedia :: Video :: Capture',
          'Topic :: Security :: Cryptography',
          'Topic :: Software Development :: Libraries :: Python Modules'
          ],
      test_suite='monkeysign.tests',
      )
