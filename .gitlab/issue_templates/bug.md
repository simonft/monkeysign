> Please provide a summary of the bug you encountered here.

## Expected behavior

> What you expected to happen

## Actual behavior

> What happened instead

## Steps to reproduce

> Try to reproduce the issue. Please describe every step you took to
> reproduce the issue. Make sure to run Monkeysign with `--debug` when
> you reproduce and paste the output below.

1. step 1
2. step 2
3. profit!

## Suggested fixes

> If you have ideas about how to fix this, feel free to provide it
> here or remove this section.

## Testsuite output

> Please paste the output of `monkeysign --test` here:

```
$ monkeysign --test
```

## Environment details

> Run `monkeysign --version`. If you are running 2.1.0, just paste the
> output here. Otherwise, paste the version number, but also:
> 
> * ``lsb_release -b``: operating system name, version and codename
>   (e.g. ``Debian GNU/Linux 8.4 (jessie)``)
> * ``uname -a``: kernel version, architecture and build dates (e.g. ``Linux
>   marcos 3.16.0-4-amd64 #1 SMP Debian 3.16.7-ckt25-1 (2016-03-06)
>   x86_64 GNU/Linux``)
> * ``python --version``
> * ``gpg --version``
>
> If you are using the graphical interface, try to also include the
> versions of the following libraries:
> 
> * GTK
> * PyGTK
> * ZBar
> * QRencode
> * PIL
>
> Also explain how was Monkeysign installed (Debian package, PIP, from git, etc).

## Debugging output

> When you reproduced the issue, you used the `--debug` flag, paste
> the output here:

```
$ monkeysign --debug ...
```
