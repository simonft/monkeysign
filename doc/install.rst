Install
=======

Monkeysign can be installed in various ways, depending on which
platform you are using. You can install Monkeysign:

* :ref:`distro-install`, recommended if you are running a distribution
  that has native packages for Monkeysign
* :ref:`pip-install`, recommended if you are on another distribution
  that doesn't have native packages of Monkeysign or you want to run
  the latest version without upgrading the whole operating system
* :ref:`source-install`, if the above doesn't work, if you need to
  test unreleased code, or if you want to contribute to Monkeysign

.. _distro-install:

Using packages
--------------

Some distributions offer ready-to-use packages for Monkeysign which
can be easily installed with a package manager. Below is a table of
distributions that have packages for Monkeysign.

.. important:: Those packages may not be up to date with the latest
               releases. Before submitting a bug report, check the
               package version and compare that to our latest release
               then review the `changelog
               <https://0xacab.org/monkeysphere/monkeysign/blob/HEAD/debian/changelog>`_
               to see if the bug has been fixed. Report bugs to the
               package maintainer rather than directly to Monkeysign
               if the package is out of date in the distribution.

               Also consider that the packages below (apart from the
               Debian packages) have not been reviewed by the
               Monkeysign team.

.. keep this list in alphabetical order

============ ============================================= =======
Distribution Source                                        Command
============ ============================================= =======
Arch Linux   `AUR`_ [1]_                                   ``pacman -S monkeysign``
Debian       `jessie`_, `stretch`_, `sid`_, ... [2]_       ``apt-get install monkeysign``
Gentoo       `ebuild`_                                     ``emerge monkeysign``
openSUSE     `openSUSE official repository`_               ``zypper in python-monkeysign``
Raspbian     `Raspbian pool`_                              ``apt-get install monkeysign``
Ubuntu       `14.04`_, `15.04`_, `15.10`_, `16.04`_, ...   ``apt-get install monkeysign``
============ ============================================= =======

.. _AUR: https://aur.archlinux.org/packages/monkeysign/
.. _wheezy: https://packages.debian.org/wheezy-backports/monkeysign
.. _jessie: https://packages.debian.org/jessie/monkeysign
.. _stretch: https://packages.debian.org/stretch/monkeysign
.. _sid: https://packages.debian.org/sid/monkeysign
.. _ebuild: https://packages.gentoo.org/packages/app-crypt/monkeysign
.. _openSUSE official repository: http://software.opensuse.org/package/python-monkeysign
.. _Raspbian pool: http://archive.raspbian.org/raspbian/pool/main/m/monkeysign/
.. _16.04: https://launchpad.net/ubuntu/xenial/+source/monkeysign
.. _15.10: https://launchpad.net/ubuntu/wily/+source/monkeysign
.. _15.04: https://launchpad.net/ubuntu/vivid/+source/monkeysign
.. _14.04: https://launchpad.net/ubuntu/trusty/+source/monkeysign

.. [1] The AUR package ships with patches that have not been reviewed
       by the Monkeysign team.
.. [2] Monkeysign has been in Debian since Debian 6
       (`squeeze-backports-sloppy`) and is maintained there as a
       native package.

.. tip:: Please ask package maintainers to build a package for your
         platform if it is missing above or, if you can package /
         submit it yourself, please help us with that! If you package
         Monkeysign, please let us know by `filing an issue
         <https://0xacab.org/monkeysphere/monkeysign/issues/new>`_
         detailing the distribution name, a link to the package and a
         command to install it.

.. _pip-install:

Using PIP
---------

You can install Monkeysign with PIP, with the following command::

  pip install monkeysign

.. important:: Note that 2.1.0 is the first release of Monkeysign
               published this way. It has not received as much testing
               as the other methods.

.. _source-install:

From source
-----------

Installing Monkeysign from source is harder, and shouldn't generally
be necessary. You may be asked, however, to do that in order to test
if your bug is still present in the current release.

Requirements
~~~~~~~~~~~~

The following Python packages are required for the GUI to work::

    python-qrencode python-gtk2 python-zbar python-zbarpygtk

If they are not available, the commandline signing tool should still
work but doesn't recognize QR codes.

Monkeysign requires a working GnuPG installation.

Downloading
~~~~~~~~~~~

You can fetch Monkeysign with git::

    git clone https://0xacab.org/monkeysphere/monkeysign.git

Tarballs are also automatically generated on the 0xACAB site for `the
main branch
<https://0xacab.org/monkeysphere/monkeysign/repository/archive.tar.gz?ref=HEAD>`_
or you can download tarballs `for every past release
<https://0xacab.org/monkeysphere/monkeysign/tags>`_ as well.

You can also find a source tarball from the Debian mirrors here::

    http://cdn.debian.net/debian/pool/main/m/monkeysign/

The ``.tar.gz`` file has a checksum, cryptographically signed, in the
``.dsc`` file.

Installing
~~~~~~~~~~

To install monkeysign from source, run::

    sudo ./setup.py install --record=install.log

Running
~~~~~~~

It is also be possible to run Monkeysign without installing it,
directly from the source tree, with::

  ./scripts/monkeysign

and::

  ./scripts/monkeyscan

See the :doc:`usage` section for more information on how to use Monkeysign.
