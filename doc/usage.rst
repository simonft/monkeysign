Usage
=====

Monkeysign comes in two different interfaces: a commandline interface
named `monkeysign` and a graphical interface named `monkeyscan`.

Monkeysign creates a temporary keyring to sign keys, and then
encrypts and sends the signature by email to the owner of the key.
This makes possible to verify that the holder of the private key (used to
decrypt the signature) has also access to the mailbox mentioned in the key.

.. note:: Make sure you have your email credentials in hand and read
          the :ref:`email-config` section before starting, as
          Monkeysign may not know, by default, how to send email.

.. tip:: If you have problems using Monkeysign, please do report
         issues and bugs about it, it's a great way of contributing!
         We also welcome documentation, translation and patches, see
         :doc:`contributing` for more information.

Monkeysign
----------

The commandline interface should provide you with a complete help file
when called with ``--help``::

    monkeysign --help

For example, to sign the Monkeysign test key::

    monkeysign 3F94 240C 918E 6359 0B04  152E 86E4 E70A 96F4 7C6A

This will fetch the key from your keyring (or a keyserver) and sign it
in a temporary keyring, then encrypt the signature and send it in an
email to the owner of the key. Emails can be sent in different ways,
documented in :ref:`email-config`.

If the wrong secret key is chosen to sign the key, you can override it
with the ``--user`` option.

.. caution:: It is important to use Monkeysign with the fingerprint,
             and *not with the key id*, specially when using it
             through the Tor network, as a key id can be duplicated
             easily, unlike fingerprints.

Monkeyscan
----------

The graphical interface (`GUI`) should be self-explanatory, it should
be in your regular application menus, or you can call start it form
the commandline with::

    monkeyscan

The GUI will show you a :term:`bar code` representing the fingerprint
of what Monkeysign thinks is your primary key. You can change that in
the `Identity` menu, or by customizing the ``default-key`` parameter
in your ``gpg.conf`` file.

On the left side, you should see the output of your camera. You can
change cameras (if you have more than one) in the `Video device` menu,
where you can also turn off the camera altogether.

To exchange fingerprints, you should point the camera at another
user's bar code. Monkeysign will detect that user's key fingerprint,
fetch the key over the network from keyservers, then ask you for
confirmation before signing and sending the email, just like the
command line interface. See :ref:`email-config` for more information
about how email is sent in Monkeysign in general.

There is a very crude preferences window available in the `Edit`
menu. There is work underway to improve it (see :issue:`41`), but it
should allow you to create a configuration file with your personal
settings. See :ref:`configuration-files` for more information about
this as well.

.. _email-config:

Sending signed key material
---------------------------

Monkeysign will attempt to send the signed key by email, unless the
``--no-mail`` argument is specified. In this case, the encrypted key
material is shown on the terminal and can then be copy-pasted in the
medium of your choice. This is useful, for example, if you use a
web email client like Roundcube, Google Mail or similar.

Monkeysign supports many ways of sending emails:

* :ref:`MTA` (e.g. sendmail or Postfix)
* :ref:`MUA` (e.g. Thunderbird or Mutt)
* :ref:`SMTP` (e.g. connecting directly to your provider)

Also note that the ``--tor`` option affects how email will be sent,
but only when using the SMTP method, as Monkeysign has no way to
handle how your :term:`MUA` or :term:`MTA` will talk to the network.

.. _mta:

Using the system email software (MTA)
`````````````````````````````````````

Monkeysign, by default, assumes you have a local :term:`MTA` installed
as ``sendmail``. If it is not, you can specify the path to a
``sendmail`` compatible program with the ``--mta`` option. Such a
program should accept the complete message on standard input, and the
recipient is passed on the commandline in place of the ``%(to)s``
argument.

Note that it is uncommon for workstations and laptops to have a
working :term:`MTA` installed: this is more commonly done on servers,
and unless you know what you are doing, you are more likely to want to
talk to your existing email client, your :term:`MUA`.

.. _mua:

Using your normal email client (MUA)
````````````````````````````````````

Therefore, to properly send email on your workstation, you may need to
tell Monkeysign how to use your regular email client, your
:term:`MUA`. For this, you can use the ``--mua`` option.

By default, Monkeysign will try to figure out your default email
client when you use the ``--mua`` flag is used without argument. This
will in turn call the ``xdg-email`` command which automatically uses
your configured email client correctly::

  monkeysign --mua [...]

Your default mail client can be modified in your desktop environment
control panel, or with the ``xdg-mime`` command, for example this will
set Thunderbird as your default email client::

  xdg-mime default thunderbird.desktop x-scheme-handler/mailto

You can also specify your own email client on the fly. Here are few
examples of known working configurations.

* Thunderbird::

    monkeysign --mua "thunderbird -compose to=%(to)s,subject=%(subject)s,body=%(body)s,attachment=%(attach)s" [...]

* Mutt::

    monkeysign --mua "mutt -a %(attach)s -s %(subject)s -i %(body)s %(to)s" [...]
  
Finally, note that you need to confirm when you are finished writing
the actual email. This is because we cannot tell when the email is
sent, because a lot of software (especially Thunderbird) return before
the email is sent, see :bts:`677430` for more information about this
issue.

.. note:: Essentially, the difference between ``--mta`` and ``--mua``
          is that the complete message is piped through the
          :term:`MTA` command whereas it is passed as an argument on
          the commandline for :term:`MUA` commands. Also, the
          ``--mta`` command expands only the ``%(to)s`` parameter,
          whereas the ``--mua`` command expands ``%(attach)s``,
          ``%(subject)s``, ``%(body)s`` and ``%(to)s``.

          Note that when a ``--mua`` is used, only the key material is
          encrypted: the body of the email is sent in the clear. This
          is because Monkeysign cannot control how the attachment
          layout in the :term:`MUA` in a standard way.

          Furthermore, it may be more difficult for the end-user to
          import the key when it was sent with a :term:`MUA`, as the
          recipient's own :term:`MUA` may not know how to both decrypt
          *and* import the key at the same time. The :term:`MTA`
          method doesn't have this problem because of MIME
          encapsulation. See also :issue:`7` for a broader technical
          discussion about the ``--mua`` implementation.

.. _smtp:

Using SMTP
``````````

Note that you can also send email using your provider's SMTP server
directly, turning Monkeysign into a :term:`MUA` itself. For example::

  monkeysign --smtp=mail.example.com:587 --smtpuser=john [fingerprint of OpenPGP key to sign]

In the above, Monkeysign will attempt to connect to the
``mail.example.com`` SMTP server over the submission port (``587``),
attempt to upgrade the connection securely (using ``STARTTLS``) and
use the `john` username. Password will be prompted securely.

.. tip:: To use a raw ``TLS`` connection, you can also use the
         ``--tls`` flag.

.. tip:: You can also try to deliver email over `Tor
         <https://www.torproject.org/>`_ network with the ``--tor``
         option. Be be aware that a lot of email providers block Tor
         exit nodes for spam control. You may need to use your
         provider's hidden service to workaround those issues. Ask
         your email provider for Tor support if you have problems with
         the SMTP method.
 
.. _configuration-files:

Configuration files
-------------------

Monkeysign will read ``/etc/monkeysign.conf`` and
``~/.config/monkeysign.conf`` (in that order) for configuration
options. Each option can be specified on its own line. Lines starting
with the pound sign (``#``) are ignored as comments. A configuration
file can be generated with the ``--save`` option, or through the
preferences window in the GUI. Here is a sample configuration file::

  # use my SMTP server to send email
  smtpserver=smtp.example.com:587
  # this is my username, password is securely prompted interactively
  smtpuser=john
  # be more verbose
  verbose

As you can see, flags like ``--verbose`` are simply specified on their
own, while options with arguments need to be seperated with an equal
(``=``) sign.
