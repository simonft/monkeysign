.. Monkeysign documentation master file, created by
   sphinx-quickstart on Sun Jan 26 15:14:59 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2

   install
   usage
   support
   contributing
   history
   api
   ui-mockups/index
   glossary
   credits

*   :ref:`genindex`
*   :ref:`modindex`
*   :ref:`search`
