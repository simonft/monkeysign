=========
 Credits
=========

Those people are the ones who made Monkeysign possible.

.. literalinclude:: ../monkeysign/__init__.py
   :start-after: credits-start
